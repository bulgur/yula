PROJECT_NAME ?= yula


.PHONY: build
build:
	docker-compose build

.PHONY: test
test: tests fmt_check coverage


.PHONY: tests
tests:
	python3 -m pytest -vv tests/*

.PHONY: test
test: tests fmt_check

.PHONY: coverage
coverage:
	echo "coverage"


.PHONY: fmt_check
fmt_check:
	python3 -m black --check yula bin tests


.PHONY: fmt
fmt:
	python3 -m black yula bin tests


doc:
	make -C docs html


clean:
	rm -rf docs/build/
	find -iname "*.pyc" | xargs -r rm -f
	find -iname "__pycache__" | xargs -r rm -rf
