import random
from yula.core import find_yubikey, find_yubikeys


def test_find_yubikeys_ony_key(yubico_find_yubikey, yubikey_mock, mocker):
    yubico_find_yubikey.side_effect = [yubikey_mock] + list(
        yubico_find_yubikey.side_effect
    )
    result = find_yubikeys()
    assert yubico_find_yubikey.call_args_list == [
        mocker.call(debug=False, skip=0),
        mocker.call(debug=False, skip=1),
    ]
    assert len(result) == 1, result
    assert result[0].serial() == 1234567


def test_find_yubikeys_multiple_keys(
    yubico_find_yubikey, yubikey_mock, random_serial_yubikey_mock, mocker
):
    yubico_find_yubikey.side_effect = [
        yubikey_mock,
        random_serial_yubikey_mock(),
        random_serial_yubikey_mock(),
    ] + list(yubico_find_yubikey.side_effect)
    result = find_yubikeys()
    assert yubico_find_yubikey.call_args_list == [
        mocker.call(debug=False, skip=0),
        mocker.call(debug=False, skip=1),
        mocker.call(debug=False, skip=2),
        mocker.call(debug=False, skip=3),
    ]
    assert len(result) == 3, result
    assert result[0].serial() == 1234567


def test_find_yubikeys_multiple_keys_limit(
    yubico_find_yubikey, yubikey_mock, random_serial_yubikey_mock, mocker
):
    yubico_find_yubikey.side_effect = [
        yubikey_mock,
        random_serial_yubikey_mock(),
        random_serial_yubikey_mock(),
    ] + list(yubico_find_yubikey.side_effect)
    result = find_yubikeys(max=1)
    assert yubico_find_yubikey.call_args_list == [mocker.call(debug=False, skip=0)]
    assert len(result) == 1, result
    assert result[0].serial() == 1234567


def test_find_yubikeys_filter(
    yubico_find_yubikey, yubikey_mock, random_serial_yubikey_mock, mocker
):
    yubikeys = [
        yubikey_mock,
        random_serial_yubikey_mock(),
        random_serial_yubikey_mock(),
    ]
    random.shuffle(yubikeys)
    yubico_find_yubikey.side_effect = yubikeys + list(yubico_find_yubikey.side_effect)
    result = find_yubikeys(serials=[1234567])
    assert yubico_find_yubikey.call_args_list == [
        mocker.call(debug=False, skip=0),
        mocker.call(debug=False, skip=1),
        mocker.call(debug=False, skip=2),
        mocker.call(debug=False, skip=3),
    ]
    assert len(result) == 1, result
    assert result[0].serial() == 1234567


def test_find_yubikeys_filter_no_match(
    yubico_find_yubikey, yubikey_mock, random_serial_yubikey_mock, mocker
):
    yubikeys = [
        yubikey_mock,
        random_serial_yubikey_mock(),
        random_serial_yubikey_mock(),
    ]
    random.shuffle(yubikeys)
    yubico_find_yubikey.side_effect = yubikeys + list(yubico_find_yubikey.side_effect)
    result = find_yubikeys(serials=[123456])
    assert yubico_find_yubikey.call_args_list == [
        mocker.call(debug=False, skip=0),
        mocker.call(debug=False, skip=1),
        mocker.call(debug=False, skip=2),
        mocker.call(debug=False, skip=3),
    ]
    assert len(result) == 0, result


def test_find_yubikey(
    yubico_find_yubikey, yubikey_mock, random_serial_yubikey_mock, mocker
):
    yubikeys = [
        yubikey_mock,
        random_serial_yubikey_mock(),
        random_serial_yubikey_mock(),
    ]
    random.shuffle(yubikeys)
    yubico_find_yubikey.side_effect = yubikeys + list(yubico_find_yubikey.side_effect)
    result = find_yubikey(1234567)
    assert yubico_find_yubikey.call_args_list == [
        mocker.call(debug=False, skip=0),
        mocker.call(debug=False, skip=1),
        mocker.call(debug=False, skip=2),
        mocker.call(debug=False, skip=3),
    ]
    assert result.serial() == 1234567


def test_find_yubikey_raise_error(
    yubico_find_yubikey, yubikey_mock, random_serial_yubikey_mock, mocker
):
    yubikeys = [
        yubikey_mock,
        random_serial_yubikey_mock(),
        random_serial_yubikey_mock(),
    ]
    random.shuffle(yubikeys)
    yubico_find_yubikey.side_effect = yubikeys + list(yubico_find_yubikey.side_effect)
    try:
        find_yubikey(12345678)
    except LookupError:
        pass
    else:
        raise AssertionError("Excepted LookupError to be raised.")
    assert yubico_find_yubikey.call_args_list == [
        mocker.call(debug=False, skip=0),
        mocker.call(debug=False, skip=1),
        mocker.call(debug=False, skip=2),
        mocker.call(debug=False, skip=3),
    ]
