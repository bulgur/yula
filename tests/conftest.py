import random
from pytest import fixture
from yubico.yubico_exception import YubicoError


@fixture
def yubico_find_yubikey(mocker):
    patch = mocker.patch("yubico.find_yubikey")
    patch.side_effect = [YubicoError("ss")]
    yield patch


@fixture
def yubikey_mock(mocker):
    yk = mocker.MagicMock()
    yk.serial.return_value = 1234567
    yield yk


@fixture
def random_serial_yubikey_mock(mocker):
    def generate():
        yk = mocker.MagicMock()
        yk.serial.return_value = random.randint(1000000, 9999999)
        return yk

    yield generate
