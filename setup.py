#!/usr/bin/env python

from setuptools import setup


setup(
    name='yula',
    url='https://gitlab.com/bulgur/yula',
    description='yubikey udev login authenticator',
    author='bulgur team',
    keywords='Yubico udev',
    version='0.1.0',
    license='MIT',
    install_requires=[
        'python-yubico',
    ],
    packages=['yula'],
)
