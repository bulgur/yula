import yubico
from yubico import yubico_exception


def find_yubikeys(serials=None, max=10):
    yubikeys = []
    for x in range(max):
        try:
            yubikey = yubico.find_yubikey(debug=False, skip=x)
            yubikeys.append(yubikey)
        except yubico_exception.YubicoError:
            if serials:
                yubikeys = [yk for yk in yubikeys if yk.serial() in serials]
            break
    return yubikeys


def find_yubikey(serial):
    yubikey = find_yubikeys(serials=[serial])
    if not yubikey:
        raise LookupError("Yubikey with given serial not found")
    return yubikey[0]
