#!/usr/bin/env python3


import os


def check_env():
    """
    Test environment for presence of mandatory variables:
        - SUBSYSTEM
        - NAME
        - MSC

    Those variables are passed by udev.
    """
    env = os.environ
    if "SUBSYSTEM" not in env or env["SUBSYSTEM"] != "input":
        return False
    if "NAME" not in env or env["NAME"].find("Yubico Yubikey") != 0:
        return False
    if "MSC" not in env or not len(env["MSC"]):
        return False
    return True


if __name__ == "__main__":
    if check_env():
        pass
    else:
        exit(0)

# vim:set et sw=4 ts=4 ft=python:
