FROM python:3.8-alpine AS builder

COPY requirements.txt /requirements.txt

RUN apk add --no-cache gcc musl-dev && pip install -r /requirements.txt

FROM python:3.8-alpine AS runner

RUN apk add --no-cache make git

COPY --from=builder /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/

ADD ./ /repo

WORKDIR /repo

RUN pip install -e .
